import { Controller, Get, Query } from '@nestjs/common';
import { VouchersService } from './vouchers.service';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
@ApiBearerAuth("access-token")
@ApiTags("vouchers")
@Controller('vouchers')
export class VouchersController {
  constructor(private readonly vouchersService: VouchersService) {}

  
@ApiQuery({name:"category",required:false})
@Get("myVouchers")
async  myVouchers(@Query("category") category:string) {
  return this.vouchersService.getMyVouchers(category);
}
}

