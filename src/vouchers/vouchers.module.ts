import { Module } from '@nestjs/common';
import { VouchersService } from './vouchers.service';
import { VouchersController } from './vouchers.controller';
import { MongoModule } from 'src/mongo/mongo.module';

@Module({
  controllers: [VouchersController],
  providers: [VouchersService],
  imports:[MongoModule],
})
export class VouchersModule {}
