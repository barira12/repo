import { Injectable } from '@nestjs/common';
import { MongoService } from 'src/mongo/mongo.service';
import { CollectionNames } from 'src/util/constants';

@Injectable()
export class VouchersService {
    constructor(private readonly mongService:MongoService){}

    async getMyVouchers(category:string) {
     try {
        let checkQuerry:any={}
        if(category != ""){
            checkQuerry={
                $and:[
                    {"category":{$eq:category}},
                    { isDeleted:{$eq:false}},
                ]
            }
        }else{
            checkQuerry={
                        isDeleted:{$eq:false}
            }
        }
      
        let getMyVouchers =await this.mongService.findWithQueryAndSort(CollectionNames.Voucher,{},"createdOn","desc",);
        if(getMyVouchers){
            return this.mongService.createApiResponse(true,getMyVouchers,"Success");
        }else{
            return this.mongService.createApiResponse(false,{},"No data found");
        }
     } catch (error) {
        return this.mongService.createApiResponse(false,"something went wrong", error);
     }
    }
}
