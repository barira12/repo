import { Injectable } from '@nestjs/common';
import { MongoBaseService, Operation } from 'packages/mongodb-crud-operations';
import { SchedulingService, StartJobDTO } from 'scheduling-bull-queues';
import { UtilService } from 'src/util/util.service';
import { LoginRequestDTO } from './DTO/login-dto';
import { LoginModelDTO } from './Model/login-model';
import { PhoneNumberModel } from './DTO/signup-dto';
import { SocialLoginRequestDTO, SocialLoginRequestsDTO } from './DTO/sociallogin-dto';
import { CollectionNames, Constants, TemplateNames } from 'src/util/constants';
import { ChangePasswordDTO } from './DTO/changepassword-dto';
import { RequestContext } from 'src/interceptors/request-context';
import { ForgotPasswordDTO } from './DTO/forgotpassword-dto';
import { ResetPassword } from './DTO/resetpassword.dto';
import { LoginWithNumberDTO } from './DTO/loginWithNumber.dto';
import { I18nContext, I18nService } from 'nestjs-i18n';
import { SignUpRequestDTO } from './DTO/signUp-request.dto';
import { Collections } from 'src/util/enum';
import { SignupModelDTO } from './Model/signUpModel';
import { UpdateUserProfile } from './DTO/updateUserProfile';
import { AuthenticationService, ForgotPasswordModel, ResetPasswordModel, ResponseModel } from 'packages/authentication-service';


@Injectable()
export class AuthService {
  constructor(
    private readonly utilservice: UtilService,
    private readonly mongoService: MongoBaseService,
    private readonly authentictionService: AuthenticationService,
    private readonly schedulingService: SchedulingService,
    private readonly i18n: I18nService,
  ) { }



  async loginUp(loginupDTO: LoginRequestDTO) {
    try {
      let loginModel: LoginModelDTO = {
        email: loginupDTO.email,
        password: loginupDTO.password,
      }
      let res = await this.authentictionService.login(loginModel);
      if (res.isSuccess == true) {
        res.data.user.isProfileCreated = true;
        let updateUser = this.utilservice.getEntityLog(res.data.user, Constants.dbName, CollectionNames.Users, Operation.Update, "", "", res.data.user._id);
        let entity = await this.mongoService.update(updateUser);
        if (res.data.user.isDeleted == true) {
          return this.utilservice.getResponseObject(false, this.i18n.t('lang.noUserFound', {
            lang: I18nContext.current().lang,
          }),);
        }
        return res;
      } else {
        return this.utilservice.getResponseObject(false, this.i18n.t('lang.invalidCredentials', {
          lang: I18nContext.current().lang,
        }),)
      }
    } catch (err) {
      return this.utilservice.getResponseObject(false, err.message, err.stack)
    }
  }


  async checkReferalCodeValid(joiningReferalCode: string) {
    try {
      let checkQuerry: any = {
        referalId: { $eq: joiningReferalCode },
      }
      let findUser = this.utilservice.getfindDataModelQuery(Constants.dbName, CollectionNames.Users, checkQuerry);
      let getUser = this.mongoService.filter(findUser);
      if (getUser) {
        return getUser;
      } else {
        return true;
      }
    } catch (err) {
      return this.utilservice.getResponseObject(false, this.i18n.t('lang.somethingwentwrong', {
        lang: I18nContext.current().lang,
      }), err)
    }
  }
  async signUp(signUpRequestDTO: SignUpRequestDTO) {
    try {
      let testingUsers = ["johndoe@mailinator.com"]
      var number; do { number = Math.floor(Math.random() * 999999); } while (number < 100000);
      let signUpModel: SignupModelDTO = {
        user: {
          email: signUpRequestDTO.emailaddress,
          password: number.toString(),
          phoneNumber: "",
          countryCode: "",
          dialCode: "",
          fullName: "",
          emailaddress: signUpRequestDTO.emailaddress,
          loginMethod: "email",
          isProfileCreated: false,
        }
      }
      let index = testingUsers.indexOf(signUpModel.user.email);
      if (index != -1) {
        signUpModel.user.password = "123456";
      }
      let res = await this.authentictionService.signUp(signUpModel);
      console.log("SIGNUP RESPONSE", JSON.stringify(res));
      if (res.isSuccess == true) {
        res.message = "User Created Successfully";
        this.sendVerifyEmail(signUpModel.user.password, signUpModel.user.email, [signUpModel.user.email]);

      }

      else if (res.isSuccess == false && res.message == "Email Already Exists") {
        console.log("EMAIL ALREADY EXISTS");
        res = await this.authentictionService.oneExistEmailOtp(signUpModel);
        console.log("NEW RES : ", JSON.stringify(res));
        this.sendVerifyEmail(signUpModel.user.password, signUpModel.user.email, [signUpModel.user.email]);

      }
      return res;

    }
    catch (err) {
      console.log(err);
      return this.utilservice.getResponseObject(false, err.message, err.stack);
    }
  }

  private async sendVerifyEmail(code: string, originalEmail: string, sendTo: string[]) {
    try {
      let emailId: string = new Date().getTime().toString();
      let email = {
        data: {
          code: code,
          email: originalEmail
        },
        templateName: TemplateNames.VerifyEmail,
        status: 'created',
        sendTo: sendTo,
        subject: 'Verify Your Email'
      };
      let emailEntityLog = this.utilservice.getEntityLog(email, Constants.dbName, Collections.emails, Operation.Update, "", "", emailId);
      await this.mongoService.update(emailEntityLog);

      let startJobData = new StartJobDTO();
      startJobData.data = {
        'emailId': emailId
      };
      startJobData.delay = 0;
      startJobData.serviceName = 'email-template';
      startJobData.uniqueId = 'email-template-' + emailId;
      await this.schedulingService.startJob(startJobData);
      email.status = "queued";
      emailEntityLog = this.utilservice.getEntityLog({ status: "queued" }, Constants.dbName, Collections.emails, Operation.Update, "", "", emailId);
      this.mongoService.update(emailEntityLog);
    }
    catch (err) {
      console.log(err);
    }
  }
  async socialLogin(socialLoginRequestDTO: SocialLoginRequestDTO) {
    try {
      let socialLoginModel: any = {
        token: socialLoginRequestDTO.token,
        user: {
          email: socialLoginRequestDTO.email,
          fullName: socialLoginRequestDTO.fullName ? socialLoginRequestDTO.fullName : "",
          phoneNumber: new PhoneNumberModel(socialLoginRequestDTO.phoneNumber.dialCode, socialLoginRequestDTO.phoneNumber.number),
          photoUrl: socialLoginRequestDTO.photoUrl ? socialLoginRequestDTO.photoUrl : socialLoginRequestDTO.photoUrl,
          platform: socialLoginRequestDTO.platform ? socialLoginRequestDTO.platform : "",
          uid: socialLoginRequestDTO.uid,
          type: socialLoginRequestDTO.type,
          isEmailVerified: true,
          isPhoneNumberVerified: true,
          userMode: socialLoginRequestDTO.userMode,

        },
      }
      let res = await this.authentictionService.socialLogin(socialLoginModel);
      if (res != null && res.isSuccess == true) {
        let updateUid = this.utilservice.getEntityLog({ uid: socialLoginModel.user.uid, type: "social", "userMode": socialLoginRequestDTO.userMode }, Constants.dbName, CollectionNames.Users, Operation.Update, "", "", res.data.user._id);
        let updaedEntity = await this.mongoService.update(updateUid);
        res.data.user = updaedEntity;
        return res;
      }
      return res;
    }
    catch (err) {
      return await this.utilservice.getResponseObject(false, err.message, err.stack);
    }
  }


  async socialLogins(SocialLoginRequestDTO: SocialLoginRequestsDTO) {
    try {
      let socialLoginModel: any = {
        // token: SocialLoginRequestDTO.token,
        user: {
          uid: SocialLoginRequestDTO.uid,
          isPhoneNumberVerified: true,
           userMode: SocialLoginRequestDTO.userMode,

        },
      }
      let res = await this.authentictionService.socialLogin(socialLoginModel);
      if (res != null && res.isSuccess == true) {
        let updateUid = this.utilservice.getEntityLog({ uid: socialLoginModel.user.uid, type: "social", "userMode": SocialLoginRequestDTO.userMode }, Constants.dbName, CollectionNames.Users, Operation.Update, "", "", res.data.user._id);
        let updaedEntity = await this.mongoService.update(updateUid);
        res.data.user = updaedEntity;
        return res;
      }
      return res;
    }
    catch (err) {
      return await this.utilservice.getResponseObject(false, err.message, err.stack);
    }
  }
  
  async changePassword(changePasswordDTO: ChangePasswordDTO) {
    try {
      changePasswordDTO.userId = RequestContext.currentUserId();
      return await this.authentictionService.changePassword(changePasswordDTO)

    }
    catch (err) {
      return this.utilservice.getResponseObject(false, err.message, err.stack);
    }
  }


  async forgotPassword(forgotPasswordRequestDTO: ForgotPasswordDTO) {
    try {
      let forgotPasswordModel: ForgotPasswordModel = {
        email: forgotPasswordRequestDTO.email,
      }

      let res = await this.authentictionService.forgotPassword(forgotPasswordModel);
      if (res && res.isSuccess) {

        await this.sendForgotPasswordEmail(res.data, forgotPasswordModel.email, [forgotPasswordModel.email]);
        res.data = "";
      }
      return res;
    }
    catch (err) {
      return this.utilservice.getResponseObject(false, err.message, err.stack);
    }
  }

  private async sendForgotPasswordEmail(code: string, originalEmail: string, sendTo: string[]) {
    try {
      let emailId: string = new Date().getTime().toString();
      let email = {
        data: {
          code: code,
          email: originalEmail
        },
        templateName: TemplateNames.VerifyEmail,
        status: 'created',
        sendTo: sendTo,
        subject: 'Verify Email'
      };
      let emailEntityLog = this.utilservice.getEntityLog(email, Constants.dbName, CollectionNames.Emails, Operation.Update, "", "", emailId);
      await this.mongoService.update(emailEntityLog);

      let startJobData = new StartJobDTO();
      startJobData.data = {
        'emailId': emailId
      };
      startJobData.delay = 0;
      startJobData.serviceName = 'email-template';
      startJobData.uniqueId = 'email-template-' + emailId;
      await this.schedulingService.startJob(startJobData);
      email.status = "queued";
      emailEntityLog = this.utilservice.getEntityLog({ status: "queued" }, Constants.dbName, CollectionNames.Emails, Operation.Update, "", "", emailId);
      this.mongoService.update(emailEntityLog);
    }
    catch (err) {
      console.log(err);
    }
  }
  async updateUserProfile(updateUserprofile: UpdateUserProfile) {
    try {
      let entityLog = this.utilservice.getEntityLog(updateUserprofile, Constants.dbName, Collections.users, Operation.Update, "",
        "", this.utilservice.getUserIdFromCurrentRequest());
      let updateUser = await this.mongoService.update(entityLog);
      return this.utilservice.getResponseObject(true, "Profile Created", updateUser)
    }

    catch (err) {
      return this.utilservice.getResponseObject(false, err.message, err.stack)
    }

  }

  async resetPassword(resetPasswordDTO: ResetPassword) {
    try {
      let resetPasswordModel: ResetPasswordModel = {
        newPassword: resetPasswordDTO.newPassword,
        token: resetPasswordDTO.token,
      }

      return await this.authentictionService.resetPassword(resetPasswordModel)
    }
    catch (err) {
      return this.utilservice.getResponseObject(false, err.message, err.stack);
    }
  }

  async getUser() {
    try {
      let findUserModel = this.utilservice.getfindDataModelId(Constants.dbName, Collections.users, this.utilservice.getUserIdFromCurrentRequest())
      let user = await this.mongoService.findOne(findUserModel);

      if (user) {

        return this.utilservice.getResponseObject(true, "Get user Successfull.", user);

      }
      else {
        return this.utilservice.getResponseObject(false, "No user Found.", {});

      }

    }
    catch (err) {

      return this.utilservice.getResponseObject(false, err.message, err.stack)
    }
  }

  async loginWithNumber(login: LoginWithNumberDTO,) {
    var response: ResponseModel;
    try {
      let data = {
        credential: login,
      };
      response = await this.authentictionService.loginWithToken(data);
      if (response.data && response.data.user) {
        return response;
      }
    } catch (err) {
      response = this.utilservice.getResponseObject(false, err.message);
    }
    return response;
  }







}



