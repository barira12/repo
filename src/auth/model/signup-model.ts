import { SignupModel, User } from "packages/authentication-service";


export class SignupModelDTO implements SignupModel {
    user: UserModelDTO;
}

export class UserModelDTO implements User {
    fullName: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    referalCode: string;
    password: string;
    photoUrl: string;
    referalId: string;
    joinedUsersByReferal: string[];
    userMode:string;

}   
