import { BaseEntityModel } from "src/baseentity.model";

export class UserModel extends BaseEntityModel{
    fullName: string;
    firstName: string;
    lastName: string;
    email: string;
    dialCode: string;
    phoneNumber: string;
    photourl:string;
    referalCode: string;
    referalId: string;
    joinedUsersByReferal: string[];
   
}

