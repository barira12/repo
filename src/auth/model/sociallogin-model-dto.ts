import { SocialLoginModel, SocialUser } from "packages/authentication-service";
import { PhoneNumberModel } from "../DTO/signup-dto";

export class SocialLoginDTO  implements SocialLoginModel{ 
    token: string;
    user: SocialUserDTO;
   
}


export class SocialUserDTO  implements SocialUser{
    email: string;
    uid: string;
    platform:string;
    fullName:string;
    phoneNumber:string;
    photoUrl:string;  
    type: string;
    isEmailVerified: boolean;
    isPhoneNumberVerified:boolean;
    userMode:string;
    dialCode: string;
    loginMethod:string;
   
    
}