import { ApiProperty } from "@nestjs/swagger";

export class PhoneNumber{
    @ApiProperty()
    dialCode : string;
    @ApiProperty()
    phoneNumber : string;
}