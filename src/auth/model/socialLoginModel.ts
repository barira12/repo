import { PhoneNumber } from "./phoneNumber_model";

export interface SocialLoginModel {
    token: string;
    user: SocialUser;
    
}
export interface SocialUser {
    email: string;
    uid: string;
    phoneNumber:PhoneNumber;
}
