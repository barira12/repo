import { SignupModel, User } from "packages/authentication-service";

export class SignupModelDTO  implements SignupModel{ 
    user: UserModelDTO;
}

export class UserModelDTO  implements User{ 
    email:string;
    phoneNumber:string;
    countryCode: string;
    dialCode: string; 
    fullName: string;
    password:string;
    emailaddress:string;
    loginMethod:string;
    isProfileCreated:boolean

    
}