import { LoginModel } from "packages/authentication-service";

export class LoginModelDTO implements LoginModel {
    email: string;
    password: string;
}