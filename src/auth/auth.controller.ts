import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, Put } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { LoginRequestDTO } from './dto/login-dto';
import { SignUpRequestDTO } from './dto/signUp-request.dto';
import { SocialLoginRequestDTO,SocialLoginRequestsDTO } from './dto/sociallogin-dto';
import { LoginWithNumberDTO } from './dto/loginWithNumber.dto';
import { UpdateUserProfile } from './dto/updateUserProfile';
import { ForgotPasswordDTO } from './dto/forgotpassword-dto';
import { ChangePasswordDTO } from './dto/changepassword-dto';
import { ResetPassword } from './dto/resetpassword.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  
  @ApiOperation({description:'sign in to continue'})
  @Post("login")
  async logIn(@Body() loginDTO: LoginRequestDTO) {
    return this.authService.loginUp(loginDTO);
  }

  @UsePipes(new ValidationPipe({ transform: true }))
  @Post("signUp")
  async signUp(@Body() signUpRequestDTO: SignUpRequestDTO) {
    return await this.authService.signUp(signUpRequestDTO);
  }


  @UsePipes(new ValidationPipe({ transform: true }))
  @Post("socialLogin")
  async socialLogin(@Body() socialLoginRequestDTO: SocialLoginRequestDTO) {
    return await this.authService.socialLogin(socialLoginRequestDTO);
  }


  @UsePipes(new ValidationPipe({ transform: true }))
  @Post("socialLoginwithnumber")
  async socialLogins(@Body() SocialLoginRequestsDTO: SocialLoginRequestsDTO) {
    return await this.authService.socialLogins(SocialLoginRequestsDTO);
  }


  @UsePipes(new ValidationPipe({ transform: true }))
   @Post("forgotPassword")
  async forgotPassword(@Body() forgotPasswordRequestDTO: ForgotPasswordDTO) {
    return await this.authService.forgotPassword(forgotPasswordRequestDTO);
  }

  @Post("user/loginWithNumber")
  async loginUser(@Body() loginDTO: LoginWithNumberDTO) {
    return await this.authService.loginWithNumber(loginDTO);
  }
  
  @ApiBearerAuth("access-token")
  @Get("getUser")
  async getUser() {
    return await this.authService.getUser();
  }


  @ApiBearerAuth("access-token")
  @Put("updateUser")
  async updateUser(@Body() updateUserProfile: UpdateUserProfile) {
    return await this.authService.updateUserProfile(updateUserProfile);

  }



   @ApiBearerAuth("access-token")
   @Put("changePassword")
   async changePassword(@Body() changePasswordDTO: ChangePasswordDTO) {
     return await this.authService.changePassword(changePasswordDTO);
   }



 @ApiOperation({ description: 'Token must be required to  received code on email' })
   @Put("resetPassword")
   async resetPassword(@Body() resetPasswordDTO: ResetPassword) {
     return await this.authService.resetPassword(resetPasswordDTO);
   }

  
}
