import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { MongoBaseModule } from 'packages/mongodb-crud-operations';
import { AuthenticationModule } from 'packages/authentication-service';
import { SchedulingModule } from 'scheduling-bull-queues';
import { UtilModule } from 'src/util/util.module';

@Module({
  controllers: [AuthController],
  providers: [AuthService],
  imports:[MongoBaseModule,AuthenticationModule,SchedulingModule,UtilModule]
})
export class AuthModule {}
