import { ApiProperty, ApiPropertyOptional,  } from "@nestjs/swagger";
import {    IsNotEmpty, IsOptional,  IsString,  } from "class-validator";
export class UpdateUserProfile {
    @ApiProperty()  
    isprofileCreated:boolean;
    @ApiProperty()  
    @IsNotEmpty() @IsString()
    fullName:string; 
    @ApiProperty()  
    @IsNotEmpty() @IsString()
    phoneNumber:string;
    @ApiProperty()  
    @IsNotEmpty() @IsString()
    emailAddress:string;
    @ApiProperty()  
    @IsNotEmpty() @IsString()
    photoUrl:string;
    @ApiProperty()  
    @IsNotEmpty() @IsString()
    dialCode:string;
          
}
