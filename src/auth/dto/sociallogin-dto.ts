import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { PhoneNumberModel } from "./signup-dto";

export class SocialLoginRequestDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string;
    @ApiProperty()
    @IsNotEmpty() @IsString()
    platform: string;
    @ApiProperty()
    @IsNotEmpty() @IsString()
    uid: string;
    @ApiProperty()
    @IsNotEmpty() @IsString()
    token: string;
    @ApiPropertyOptional()
    @IsOptional() @IsString()
    fullName: string;
    @ApiPropertyOptional()
    phoneNumber: PhoneNumberModel;
    @ApiPropertyOptional()
    @IsOptional() @IsString()
    photoUrl: string;
    @ApiProperty()
    type: string;
    @ApiProperty()
    userMode: string;


}


export class SocialLoginRequestsDTO {
   
    
    token: string;
    
     phoneNumber: PhoneNumberModel;

    userMode: string;
  
    uid: string;

  
  

}
