import { ApiProperty, ApiPropertyOptional, } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, } from "class-validator";


export class PhoneNumberModel {
    @ApiProperty()
    dialCode: string;
    @ApiProperty()
    number: string;
    constructor(dialCode: string, number: string) {
        this.dialCode = dialCode;
        this.number = number
    }
}
export class SignupDTO {
    
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string;
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    phoneNumber: PhoneNumberModel;
    

}


