import { ApiProperty, ApiTags } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class SignUpRequestDTO {
@ApiProperty()
@IsNotEmpty() @IsString()   
emailaddress:string;
@ApiProperty()
phoneNumber:string;
@ApiProperty()
countryCode:string;
@ApiProperty()
dialCode:string;


} 