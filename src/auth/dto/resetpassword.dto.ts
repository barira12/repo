import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

    export class ResetPassword{
        @ApiProperty()  
        @IsNotEmpty() @IsString()
        token:string;
        @ApiProperty()  
        @IsNotEmpty() 
        newPassword:string;
    }   
