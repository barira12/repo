import { ApiProperty } from "@nestjs/swagger/dist/decorators/api-property.decorator";
import { SocialLoginModel, SocialUser } from "packages/authentication-service";

export class SocialLoginRequestModel implements SocialLoginModel {
    @ApiProperty()
    token: string;
    @ApiProperty({properties:{"email":{type:"string"},"uid":{type:"string"}}})
    user: SocialUser;
}
