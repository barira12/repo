import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmail } from "class-validator";


export class LoginWithNumberDTO {
    @ApiPropertyOptional()
    @IsEmail()
    email: string;
    @ApiPropertyOptional()
    phoneNumber:string;
}