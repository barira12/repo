import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from "class-validator";

export class ChangePasswordDTO{
    userId: string
    @ApiProperty() 
    @IsNotEmpty()
    @IsEmail() 
    currentPassword: string;
    @ApiProperty() 
    @IsNotEmpty()
    @IsEmail()
    newPassword: string;
}
