import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId, SortDirection, WithId } from 'mongodb';
import { CollectionNames } from 'src/util/constants';

@Injectable()
export class MongoService {
    findManyById() {
        throw new Error('Method not implemented.');
    }
    
    constructor(
        @Inject("mongoinstance") private readonly db: Db
    ) { }

    async findById<T>(collectionName: CollectionNames, id: string): Promise<T> {
        const response = await this.db.collection(collectionName).find({ _id: id, "$or": [{ isDeleted: false }, { isDeleted: { $exists: false } }] } as any).toArray() as T[];
        return response[0];
    }

    async findByQuery<T>(collectionName: CollectionNames, query: any): Promise<T[]> {
        const response = await this.db.collection(collectionName).find(query).toArray() as T[];
        return response;
    }

    async insert<T>(collectionName: CollectionNames, data: T) {
        data["_id"] = Date.now().toString();
        data["createdOn"] = Date.now();
        data["isDeleted"] = false;
        const response = await this.db.collection(collectionName).insertOne(data);
        data["_id"] = response.insertedId;
        return data;
    }

    async bulkInsert<T>(collectionName: CollectionNames, data: T[]) {
        const bulk = this.db.collection(collectionName).initializeUnorderedBulkOp();
        for (let item of data) {
            item["_id"] = Date.now().toString();
            item["createdOn"] = Date.now();
            item["isDeleted"] = false;
            bulk.insert(item);
        }
        const response = await bulk.execute();
        return response.insertedCount;
    }

    async update<T>(collectionName: CollectionNames, data: T, id: string, upsert: boolean = true) {
        delete data["_id"];
        data["modifiedOn"] = new Date().getTime();
        await this.db.collection(collectionName).updateOne({ _id: id } as any, { "$set": data }, { upsert: upsert });
        data["_id"] = id;
        return data;
    }

    async upsertWithID<T>(collectionName: CollectionNames, data: T, id: string, upsert: boolean = true) {
        data["modifiedOn"] = new Date().getTime();
        await this.db.collection(collectionName).updateOne({ _id: id } as any, { "$set": data }, { upsert: upsert });
        var res = await this.findById(collectionName, id);
        data = Object.assign(data, res);
        data["_id"] = id;
        return data;
    }

    async delete(collectionName: CollectionNames, id: string) {
        return await this.update(collectionName, { isDeleted: true }, id);
    }

    async countDocuments(collectionName: CollectionNames): Promise<number> {
        try {
            const count = await this.db.collection(collectionName).countDocuments();
            return count;
        } catch (error) {
            console.error('Error while counting documents:', error);
            throw error;
        }
    }

    async countByQuery(collectionName: CollectionNames, query: any): Promise<number> {
        try {
            const count = await this.db.collection(collectionName).countDocuments(query);
            return count;
        } catch (error) {
            console.error('Error while counting documents:', error);
            throw error;
        }
    }

    public createApiResponse(isSuccess?: boolean, data?: any, message?: string) {
        return {
            isSuccess: isSuccess,
            data: data,
            message: message || "", // Use an empty string if message is not provided
        };
    }
    async findWithQueryAndSort<T>(
        collectionName: CollectionNames,
        query: any,
        sortField: string,
        sortType: 'asc' | 'desc'
    ): Promise<WithId<T>[]> {
        const sortDirection: SortDirection = sortType === 'desc' ? -1 : 1;
        var sortObj: any = {};
        if (sortField) {
            sortObj[sortField] = sortDirection;
        }
        const collection: Collection<T> = this.db.collection<T>(collectionName);
        const response = await collection
            .find(query)
            .sort(sortObj)
            .toArray();

        return response;
    }


    async findWithQuerySortAndPagination<T>(
        collectionName: CollectionNames,
        query: any,
        sortField: string,
        sortType: 'asc' | 'desc',
        pageNumber: number,
        pageSize: number
    ): Promise<{ data: WithId<T>[], totalCount: number }> {
        const sortDirection: SortDirection = sortType === 'desc' ? -1 : 1;
        const skip = (pageNumber - 1) * pageSize;

        const collection: Collection<T> = this.db.collection<T>(collectionName);

        const [data, totalCount] = await Promise.all([
            collection
                .find(query)
                .sort({ [sortField]: sortDirection })
                .skip(skip)
                .limit(pageSize)
                .toArray(),
            collection.countDocuments(query)
        ]);

        return { data, totalCount };
    }
    
}



