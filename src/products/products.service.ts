import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { RequestContext } from 'src/interceptors/request-context';
import { MongoService } from 'src/mongo/mongo.service';
import { CollectionNames, Constants } from 'src/util/constants';
import { AddProductDTO } from './DTO/addproducts.dto';
import { UtilService } from 'src/util/util.service';
import { MongoBaseService } from 'packages/mongodb-crud-operations';

@Injectable()
export class ProductsService {
    constructor(private readonly mongoService: MongoService,
        @Inject("marketservice-tcp") private readonly tcpService: ClientProxy,
        private readonly utilService: UtilService,
        private readonly mongobaseservice: MongoBaseService,
    ) { }

    async createInex() {
        let findProduct = this.utilService.getfindDataModelQuery(Constants.dbName, CollectionNames.products, { location: "2dsphere" });
        await this.mongobaseservice.locationIndex(findProduct);
    }

    async addProduct(addProductDTO: AddProductDTO) {
        addProductDTO.ownerId = RequestContext.currentUserId();
        let response = await this.tcpService.send(Constants.ADD_PRODUCT_PATTERN, addProductDTO).toPromise();
        return response;
    }
    async getProducts() {
        try {
            let checkQuerry: any = {
             $and:[
                {category:{$eq:'deal'}},
                {isDeleted: { $eq: false }},
             ],
                
            }
            let findProducts = await this.mongoService.findWithQueryAndSort(CollectionNames.products, checkQuerry,"createdOn","desc");
            if (findProducts) {
                return this.mongoService.createApiResponse(true, findProducts, "Successfully data found");
            } else {
                return this.mongoService.createApiResponse(false, {},"No data found");
            }
        } catch (err) { 

            return this.mongoService.createApiResponse(false,"Something went wrong")
        }
    }


}



