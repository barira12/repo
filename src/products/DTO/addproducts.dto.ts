import { ApiProperty } from "@nestjs/swagger";
import { VariationDTO } from "./variation.dto";

export enum MarketItemType {
    Product = "product",
    Service = "service"
}

export enum ProductType {
    Simple = "simple",
    Variable = "variable"
}

export class DocumentModel {
    @ApiProperty()
    name: string;
    @ApiProperty()
    type: string;
    @ApiProperty()
    url: string;
}


export class AddProductDTO {
    @ApiProperty()
    title: string;
    @ApiProperty({ isArray: true })
    keyFeatures: string;
    @ApiProperty()
    description: string;
    @ApiProperty()
    sku: string;
    @ApiProperty()
    productWeight: string;
    @ApiProperty()
    productLength: string;
    @ApiProperty()
    productWidth: string;
    @ApiProperty()
    productHeight: string;
    @ApiProperty()
    category: string;
    @ApiProperty()
    itemType: MarketItemType;
    @ApiProperty()
    productType: ProductType;
    @ApiProperty({ isArray: true, type: VariationDTO })
    variations: VariationDTO[];
    @ApiProperty({ isArray: true, type: DocumentModel })
    media: DocumentModel;
    ownerId: string;
    @ApiProperty()
    processingDays:string;
    @ApiProperty()
    storeId:string;
    @ApiProperty()
    startTime:string;
    @ApiProperty()
    endTime:string;
    @ApiProperty()
    productMaterial:string;
    @ApiProperty()
    productShape:string;
    
}
