import { ApiProperty } from "@nestjs/swagger";

export class VariationDTO {
    @ApiProperty()
    productId: string;
    @ApiProperty()
    quantity: number;
    @ApiProperty()
    price: number;
    @ApiProperty()
    salesPrice: number;
    @ApiProperty()
    attributes: any;
    variationId: string;
    soldItems: number;
    startDate: number;
    endDate: number;
    _id:string;
    createdOn:number;
    modifiedOn:number;
    modifiedBy:string;
    createdBy:string;
    isDeleted:boolean;
    ownerId:string;
}