import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { MongoModule } from 'src/mongo/mongo.module';
import { MongoBaseModule } from 'packages/mongodb-crud-operations';
import { UtilModule } from 'src/util/util.module';

@Module({
  controllers: [ProductsController],
  imports:[MongoModule,ConfigModule,MongoBaseModule,UtilModule],
  providers: [ProductsService, {
    provide: 'marketservice-tcp',
    inject: [ConfigService],
    useFactory: (configService: ConfigService) => {
      let port = configService.get('MARKETPLACE_SERVICE_PORT');
      return ClientProxyFactory.create({ transport: Transport.TCP, options: { host: 'host.docker.internal', port: parseInt(port) } })
    },
  }]
})
export class ProductsModule { } 
