import { Body, Controller, Get, Post } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ApiTags } from '@nestjs/swagger';
import { AddProductDTO } from './DTO/addproducts.dto';


@ApiTags("products")
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) { }

  @Post("index")
  async index() {
    return this.productsService.createInex();
  }
  
  @Post("addProducts")
  addProducts(@Body() addProductDTO: AddProductDTO) {
      return this.productsService.addProduct(addProductDTO);
    }
    @Get()
    async getProducts(){
      return this.productsService.getProducts();
     }

}
