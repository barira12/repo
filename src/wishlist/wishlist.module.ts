import { Module } from '@nestjs/common';
import { WishlistService } from './wishlist.service';
import { WishlistController } from './wishlist.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

@Module({
  controllers: [WishlistController],
  providers: [
    WishlistService,
    {
      provide: 'marketservice-tcp',
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        let port = configService.get('MARKETPLACE_SERVICE_PORT');
        return ClientProxyFactory.create({
          transport: Transport.TCP,
          options: { host: 'host.docker.internal', port: parseInt("3000")},
        });
      },
    },
  ],
  imports: [ConfigModule],

})
export class WishlistModule { }
