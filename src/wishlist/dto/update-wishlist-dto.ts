import { PartialType } from '@nestjs/swagger';
import { WishlistDto } from './create-wishlist-dto';

export class UpdateCartDto extends PartialType(WishlistDto) {}
