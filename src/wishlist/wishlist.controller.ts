import { Body, Controller, Post } from '@nestjs/common';
import { WishlistService } from './wishlist.service';
import { ApiBearerAuth, ApiBody } from '@nestjs/swagger';
import { WishlistDto } from './dto/create-wishlist-dto';

@Controller('wishlist')
export class WishlistController {
  constructor(private readonly wishlistService: WishlistService) {}


  @ApiBearerAuth("access-token")
  @Post("product")
  addProductToCart(@Body() wishlistDto: WishlistDto) {
    return this.wishlistService.addProductToWishlist(wishlistDto);
  }
   
  @ApiBearerAuth("access-token")
  @ApiBody({ schema: { 'properties': { 'productId': { 'type': 'string' }, 'variationId': { 'type': 'string' } } } })
  @Post("deleteproduct")
  removeProductFromCart(@Body("productId") productId: string, @Body("variationId") variationId: string) {
    return this.wishlistService.removeProductFromWishlist(productId, variationId);
  }

}
