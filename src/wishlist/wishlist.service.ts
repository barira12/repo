import { Inject, Injectable } from '@nestjs/common';
import { WishlistDto } from './dto/create-wishlist-dto';
import { RequestContext } from 'src/interceptors/request-context';
import { Constants } from 'src/util/constants';
import { ClientProxy } from '@nestjs/microservices';


@Injectable()
export class WishlistService {

    constructor(
        @Inject("marketservice-tcp") private readonly tcpService: ClientProxy
      ) { }

      async getUserWishlist() {
        let response = await this.tcpService.send(Constants.GET_USER_WISHLIST,
          RequestContext.currentUserId()).toPromise();
        return response;
      }

    async addProductToWishlist(wishlistDto:WishlistDto) {
        let response = await this.tcpService.send(Constants.ADD_PRODUCT_TO_WISHLIST,
          { ownerId: RequestContext.currentUserId(), product: wishlistDto }).toPromise();
        return response;
      }

      async removeProductFromWishlist(productId: string, variationId: string) {
        let response = await this.tcpService.send(Constants.REMOVE_PRODUCT_FROM_WISHLIST,
          { ownerId: RequestContext.currentUserId(), productId: productId, variationId: variationId }).toPromise();
        return response;
      }
  
}
