import { ApiProperty } from "@nestjs/swagger";

export class CartDto {
    @ApiProperty()
    _id: string;
    @ApiProperty()
    createdOn: number;
    @ApiProperty()
    modifiedOn: number;
    @ApiProperty()
    modifiedBy: string;
    @ApiProperty()
    createdBy: string;
    @ApiProperty()
    isDeleted: boolean;
    @ApiProperty()
    ownerId: string;

}
