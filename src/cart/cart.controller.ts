import { Controller, Get, Post, Body, Delete, Inject,} from '@nestjs/common';
import { CartService } from './cart.service';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { CartDto } from './dto/create-cart.dto';
import { ClientProxy } from '@nestjs/microservices';
@ApiBearerAuth("access-token")
@ApiTags("cart")
@Controller('cart')
export class CartController {
  constructor(private readonly cartService: CartService,
    @Inject('marketservice-tcp') private readonly tcpService: ClientProxy,
    ) { }

  @ApiBearerAuth("access-token")
  @Get()
  getusercart() {
    return this.cartService.getusercart();
  }

  @ApiBearerAuth("access-token")
  @Post("product")
  addProductToCart(@Body() CartDto: CartDto) {
    return this.cartService.addProductToCart(CartDto);
  }
   
  @ApiBearerAuth("access-token")
  @ApiBody({ schema: { 'properties': { 'productId': { 'type': 'string' }, 'variationId': { 'type': 'string' } } } })
  @Post("deleteproduct")
  removeProductFromCart(@Body("productId") productId: string, @Body("variationId") variationId: string) {
    return this.cartService.removeProductFromCart(productId, variationId);
  }

  @ApiBearerAuth("access-token")
  @ApiBody({ schema: { 'properties': { 'productId': { 'type': 'string' }, 'variationId': { 'type': 'string' } } } })
  @Post("harddeleteproduct")
  hardDeleteProductFromCart(@Body("productId") productId: string, @Body("variationId") variationId: string){
    return this.cartService.deleteProductFromCart(productId,variationId);
  }

  @ApiBearerAuth("access-token")
  @Delete()
  clearCart() {
    return this.cartService.clearCart();
  }

}
