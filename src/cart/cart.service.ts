import { Inject, Injectable } from '@nestjs/common';
import { CartDto } from './dto/create-cart.dto';
import { ClientProxy } from '@nestjs/microservices';
import { Constants } from 'src/util/constants';
import { RequestContext } from 'src/interceptors/request-context';

@Injectable()
export class CartService {
  constructor(
    @Inject("marketservice-tcp") private readonly tcpService: ClientProxy
  ) { }

  async getusercart() {
    let response = await this.tcpService.send(Constants.GET_USER_CART,
      RequestContext.currentUserId()).toPromise();
    return response;
  }

  async addProductToCart(cartProductDTO: CartDto) {
    let response = await this.tcpService.send(Constants.ADD_PRODUCT_TO_CART,
      { ownerId: RequestContext.currentUserId(), product: cartProductDTO }).toPromise();
    return response;
  }

  async removeProductFromCart(productId: string, variationId: string) {
    let response = await this.tcpService.send(Constants.REMOVE_PRODUCT_CART,
      { ownerId: RequestContext.currentUserId(), productId: productId, variationId: variationId }).toPromise();
    return response;
  }

  async deleteProductFromCart(productId: string, variationId: string) {
    let response = await this.tcpService.send(Constants.DELETE_PRODUCT_FROM_CART,
      { ownerId: RequestContext.currentUserId(), productId: productId, variationId: variationId }).toPromise();
    return response;
  }

  async clearCart() {
    let response = await this.tcpService.send(Constants.CLEAR_CART,
      RequestContext.currentUserId()).toPromise();
    return response;
  }




}
