import { Module } from '@nestjs/common';
import { CartService } from './cart.service';
import { CartController } from './cart.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

@Module({
  controllers: [CartController],
  providers: [
    CartService,
    {
      provide: 'marketservice-tcp',
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        let port = configService.get('MARKETPLACE_SERVICE_PORT');
        return ClientProxyFactory.create({
          transport: Transport.TCP,
          options: { host: 'host.docker.internal', port: parseInt("3000")},
        });
      },
    },
  ],
  imports: [ConfigModule],

})
export class CartModule { }
