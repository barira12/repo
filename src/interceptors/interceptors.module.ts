import { Module } from '@nestjs/common';
import { UtilModule } from 'src/util/util.module';
import { RequestMiddleware } from './request.middleware';
import { AuthenticationModule } from 'packages/authentication-service';

@Module({
    providers: [RequestMiddleware],
    imports: [AuthenticationModule,UtilModule]
})
export class InterceptorsModule { }
