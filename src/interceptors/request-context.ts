import { HttpException, HttpStatus } from "@nestjs/common";
import * as cls from 'cls-hooked';
export class RequestContext {

    public static nsid = new Date().getTime().toString();
    public readonly id: Number;
    public request: Request;
    public response: Response;

    constructor(request: Request, response: Response) {
        this.id = Math.random();
        this.request = request;
        this.response = response;
    }
 
    public static currentRequestContext(): RequestContext {
        const session = cls.getNamespace(RequestContext.nsid);
        if (session && session.active) {
            return session.get(RequestContext.name);
        }

        return null;
    }

    public static currentRequest(): Request {
        let requestContext = RequestContext.currentRequestContext();

        if (requestContext) {
            return requestContext.request;
        }

        return null;
    }

    public static currentUser(throwError?: boolean) { 
        let requestContext = RequestContext.currentRequestContext();

        if (requestContext) {
            const user: any = requestContext.request['user'];
            if (user) {
                return user;
            }
        }

        if (throwError) {
            throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
        }

        return null;
    }
    public static currentUserId(throwError?: boolean) {
        let requestContext = RequestContext.currentRequestContext();

        if (requestContext) {
            const user: any = requestContext.request['userId'];
            if (user) {
                return user;
            }
        }

        if (throwError) {
            throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
        }

        return null;
    }
    
}