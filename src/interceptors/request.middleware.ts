import { HttpException, HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction } from 'express';
import { UtilService } from 'src/util/util.service';
import { RequestContext } from './request-context';
import { AuthenticationService, ValidateTokenModel } from 'packages/authentication-service';
import * as cls from 'cls-hooked';

@Injectable()
export class RequestMiddleware implements NestMiddleware {

    constructor(
        private readonly authenticationService: AuthenticationService,
        private readonly utilService: UtilService
    ) {
    }
 
    async use(req: any, res: any, next: NextFunction) {
        // try {
            // if (!Object.keys(req.body).length && req.url !== "/file/upload") {
            //     const raw = await rawbody(req);
            //     const text = raw.toString().trim();
            //     if (text) {
            //         req.body = JSON.parse(text);
            //     }
            // }
            const authorizationHeaader = req.headers.authorization;
            if (authorizationHeaader) {
                if (authorizationHeaader.split(" ")[0] == "Bearer") {
                    const token = req.headers.authorization.split(' ')[1];
                    let validateTokenModel: ValidateTokenModel = {
                        token: token
                    };
                    let response = await this.authenticationService.validateToken(validateTokenModel);
                    if (response.isSuccess) {
                        req.userId = response.data;
                    }
                    else {
                        throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);

                    }
                }
                else if (authorizationHeaader.split(" ")[0] == "Firebase") {
                    const token = req.headers.authorization.split(' ')[1];
                    let validateTokenModel: ValidateTokenModel = {
                        token: token
                    };
                    validateTokenModel["type"] = "firebase";
                    let response = await this.authenticationService.validateToken(validateTokenModel);
                    if (response.isSuccess) {
                        req.userId = response.data;
                    }
                    else {
                        throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);

                    }
                }
            }
            else {
                 throw new HttpException("Unauthorized.", HttpStatus.UNAUTHORIZED);
          
            }

            const requestContext = new RequestContext(req, res);
            const session = cls.getNamespace(RequestContext.nsid) || cls.createNamespace(RequestContext.nsid);

            try{
                session.run(async () => {
                    session.set(RequestContext.name, requestContext);
                    next();
                })
            }catch(err){
               return err;
            }
           
        // }
        //   catch (err) {
        //     // return this.utilService.getResponseObject(false, err.message, err.stack);
       
        // }

    }
}
