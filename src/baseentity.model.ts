export class BaseEntityModel{
    _id:string;
    createdOn:number;
    modifiedOn:number;
    modifiedBy:string;
    createdBy:string;
    isDeleted:boolean;
    ownerId:string;

    constructor(ownerId:string){
        this._id = new Date().getTime().toString();
        this.createdOn = new Date().getTime();
        this.modifiedOn = new Date().getTime();
        this.createdBy = "System";
        this.modifiedBy = "System";
        this.isDeleted = false;
        this.ownerId = ownerId;
    }
}