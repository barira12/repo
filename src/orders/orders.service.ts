import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { MongoService } from 'src/mongo/mongo.service';
import { AddOrderDTO } from './DTO/addorder.dto';
import { RequestContext } from 'src/interceptors/request-context';
import { CollectionNames, Constants } from 'src/util/constants';
import { CancelOrderDTO } from './DTO/cancelorder.dto';
import { CreateOrderInvoiceDTO } from './DTO/createinvoice.dto';
import { AddReviewDTO } from './DTO/addreview.dto';

@Injectable()
export class OrdersService {
    constructor(private readonly mongoService: MongoService, @Inject("marketservice-tcp") private readonly tcpService: ClientProxy,) { }

    async addOrder(addOrderDTO: AddOrderDTO) {
        addOrderDTO.userId = RequestContext.currentUserId();
        let response = await this.tcpService.send(Constants.ADD_ORDER_PATTERN, addOrderDTO).toPromise();
        return response;
    }

    async cancelOrder(cancelOrderDTO: CancelOrderDTO) {
        cancelOrderDTO.userId = RequestContext.currentUserId();
        let response = await this.tcpService.send(Constants.CANCEL_ORDER_PATTERN, cancelOrderDTO).toPromise();
        return response;
    }
    async getOrderById(orderId: string) {
        try {
            let findOrder = await this.mongoService.findById(CollectionNames.orders, orderId);
            if (findOrder) {
                return this.mongoService.createApiResponse(true, findOrder, "successfully found");
            } else {
                return this.mongoService.createApiResponse(false, {}, "something went wrong");
            }
        } catch (error) {
            return this.mongoService.createApiResponse(false, error, "something went wrong");
        }
    }

  //  async createOrderInvoice(data: CreateOrderInvoiceDTO) {
    //    let response = await this.tcpService.send(Constants.CREATE_ORDER_INVOICE_PATTERN, data).toPromise();
      //  return response;
    //}

    async getUserOrders(status: string, page: number, count: number,
        // orderType: string
    ) {
        let response = await this.tcpService.send(Constants.GET_USER_ORDERS_PATTERN, {
            status,
            //  orderType,
            page, count, userId: RequestContext.currentUserId()
        }).toPromise();
        return response;
    }

    async addReview(addReviewDTO: AddReviewDTO) {
        addReviewDTO.ownerId = RequestContext.currentUserId();
        let response = await this.tcpService.send(Constants.ADD_REVIEW_PATTERN, addReviewDTO).toPromise();
        return response;
    }

    async getReviewByOrderId(orderId: string) {
        try {
            let checkQuerry: any = {
                $and: [
                    // { isDeleted: { $eq: false } },
                    { orderId: { $eq: orderId } }
                ]
            }
            let findReviewByOrderId = await this.mongoService.findByQuery(CollectionNames.reviews, checkQuerry);
            if (findReviewByOrderId) {
                return this.mongoService.createApiResponse(true, findReviewByOrderId, "Success");
            } else {
                return this.mongoService.createApiResponse(false, {}, "No data found");
            }

        } catch (error) {
            return this.mongoService.createApiResponse(false, error, "something went wrong");
        }
    }
 
}
