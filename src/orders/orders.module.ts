import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { MongoModule } from 'src/mongo/mongo.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

@Module({
  controllers: [OrdersController],
  providers: [OrdersService, {
    provide: 'marketservice-tcp',
    inject: [ConfigService],
    useFactory: (configService: ConfigService) => {
      let port = configService.get('MARKETPLACE_SERVICE_PORT');
      return ClientProxyFactory.create({ transport: Transport.TCP, options:
         { host: 'host.docker.internal', port: parseInt("3000") } })
    },
  }],
  imports:[MongoModule,ConfigModule]
})
export class OrdersModule {}
