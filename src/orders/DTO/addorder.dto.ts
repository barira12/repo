import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { AddProductDTO } from "src/products/DTO/addproducts.dto";
import { OrderStatus } from "src/util/constants";

export class AddressOrderModel {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    fullName: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    mobileNo: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    area: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    address: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    landmark: string;
    @ApiProperty({ isArray: true })
    addressLabels: string[];
}

export enum OrderPaymentMethods {
    PayViaCreditCard = "Pay Via Credit Card / Debit Card",
    PayViaCrypto = "Pay Via Crypto",
    PayViaDiaspora = "Pay Via Diaspora",
    PayViaApplePay = "Pay Via Apple Pay",
    PayViaGooglePay = "Pay Via Google Pay",
    PayViaPayPal = "Pay Via Pay Pal",
    PayViaWesternUnion = "Pay Via Western Union",
    PayViaPerfectMoney = "Pay Via Perfect Money",
    PayViaSkrill = "Pay Via Skrill",
    PayViaNetbanking = "Pay Via Netbanking",
    PayViaMPESA = "Pay Via MPESA"

}
export class PromoCodeModel {
    @ApiProperty()
    _id: string;
    @ApiProperty()
    createdOn: number;
    @ApiProperty()
    modifiedOn: number;
    @ApiProperty()
    modifiedBy: string;
    @ApiProperty()
    createdBy: string;
    @ApiProperty()
    isDeleted: boolean;
    @ApiProperty()
    ownerId: string;
    @ApiProperty()
    name: string;
    @ApiProperty()
    endDate: number;
    @ApiProperty()
    isPercentage: boolean;
    @ApiProperty()
    value: number;
    @ApiProperty()
    minimum: number;
    @ApiProperty()
    maximum: number;
}


export class OrderSummaryModel {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    subTotal: number;
    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    shippingCharges: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    totalPrice: number;
    @ApiProperty()
    promoCode: PromoCodeModel;
}
export class OrderUserDetailModel {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    userId: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    fullName: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    phoneNumber: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    emailAddress: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    profilePicture: string;
}
export class OrderStoreModel {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    storeId: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    storeName: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    storeProfilePicture: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    storeOwnerId: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    storeOwnerName: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    storeOwnerProfilePicture: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    storeEmailAddress: string;
}

export class CartProductDTO extends AddProductDTO {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    _id: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    createdOn: number;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    modifiedOn: number;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    modifiedBy: string;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    createdBy: string;
    @ApiProperty()
    isDeleted: boolean;
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    ownerId: string;
}
export class AddOrderDTO {
    @ApiProperty({ isArray: true, type: CartProductDTO })
    items: CartProductDTO[];
    @ApiProperty()
    orderSummary: OrderSummaryModel;
    @ApiProperty()
    shippingAddress: AddressOrderModel;
    @ApiProperty()
    billingAddress: AddressOrderModel;
    @ApiProperty()
    emailAddress: string;
    @ApiProperty()
    sellerDetails: OrderStoreModel;
    @ApiProperty()
    userDetails: OrderUserDetailModel;
    @ApiProperty()
    status: OrderStatus;
    @ApiProperty()
    paymentMethod: OrderPaymentMethods;
    @ApiProperty()
    paymentMethodData: any;
    userId: string;
    @ApiProperty()
    serviceData: any;
}