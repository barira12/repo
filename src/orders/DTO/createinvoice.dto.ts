import { ApiProperty } from "@nestjs/swagger";

export class CreateOrderInvoiceDTO{
    @ApiProperty()
    orderNumber:string;
    @ApiProperty()
    itemQuantity:number;
    @ApiProperty()
    pickup:string;
    @ApiProperty()
    delivery:string;
    @ApiProperty()
    trackingId:string;
    @ApiProperty()
    orderId:string;
}