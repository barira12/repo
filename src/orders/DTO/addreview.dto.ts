import { ApiProperty } from "@nestjs/swagger";

export class AddReviewDTO {
    ownerId: string;
    @ApiProperty()
    userName: string;
    @ApiProperty()
    userProfilePicture: string;
    @ApiProperty()
    review: string;
    @ApiProperty()
    stars: number;
    @ApiProperty()
    sellerId: string;
    @ApiProperty()
    orderId: string;
}