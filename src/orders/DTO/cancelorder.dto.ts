import { ApiProperty } from "@nestjs/swagger";

export class CancelOrderDTO {
    @ApiProperty()
    orderId: string;
    userId: string;
    @ApiProperty()
    reason: string;
}