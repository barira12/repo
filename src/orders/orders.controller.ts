import { Body, Controller, DefaultValuePipe, Get, Param, Post, Put, Query } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AddOrderDTO } from './DTO/addorder.dto';
import { CancelOrderDTO } from './DTO/cancelorder.dto';
import { CreateOrderInvoiceDTO } from './DTO/createinvoice.dto';
import { AddReviewDTO } from './DTO/addreview.dto';

@ApiBearerAuth("access-token")
@ApiTags("orders")
@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) { }
  @Get("reviewByorderId")
  async reviewByOrder(@Query("orderId") orderId:string) {
    return this.ordersService.getReviewByOrderId(orderId);
  }


  @Post("order")
  addOrder(@Body() addOrderDTO: AddOrderDTO) {
    return this.ordersService.addOrder(addOrderDTO);
  }


  @Put("cancel")
  cancelOrder(@Body() cancelOrderDTO: CancelOrderDTO) {
    return this.ordersService.cancelOrder(cancelOrderDTO);
  }


  @Get("orderbyid/:id")
  orderById(@Param("id") id: string) {
    return this.ordersService.getOrderById(id);
  }

//@Post("invoice")
  //createInvoice(@Body() data: CreateOrderInvoiceDTO) {
    //return this.ordersService.createOrderInvoice(data);
  //}

  @Get("all")
  getOrders(@Query("status") status: string, @Query("page", new DefaultValuePipe(1)) page: number, @Query("count", new DefaultValuePipe(1000)) count: number, 

  ) {
    return this.ordersService.getUserOrders(status, page, count,
      
       );
  }

  @Post("review")
  addReview(@Body() addReviewDTO: AddReviewDTO) {
    return this.ordersService.addReview(addReviewDTO);
  }


}
