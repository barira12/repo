import { Module } from '@nestjs/common';
import { DisputesService } from './disputes.service';
import { DisputesController } from './disputes.controller';
import { MongoModule } from 'src/mongo/mongo.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

@Module({
  controllers: [DisputesController],
  providers: [DisputesService, {
    provide: 'marketservice-tcp',
    inject: [ConfigService],
    useFactory: (configService: ConfigService) => {
      let port = configService.get('MARKETPLACE_SERVICE_PORT');
      return ClientProxyFactory.create({ transport: Transport.TCP, options:
         { host: 'host.docker.internal', port: parseInt("") } })
    },
  }],
  imports:[MongoModule,ConfigModule]
})
export class DisputesModule {}
