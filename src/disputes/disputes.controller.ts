import { Controller, Get } from '@nestjs/common';
import { DisputesService } from './disputes.service';

@Controller('disputes')
export class DisputesController {
  constructor(private readonly disputesService: DisputesService) {}

  @Get()
  async getDisputes(){
    return this.disputesService.getDisputes();
   }

   async getDisputeDetail(){
    return this.disputesService.getDisputeDetail();
   }

}
