import { ApiProperty } from "@nestjs/swagger";

export class CreateDisputeDto {

    @ApiProperty()
    orderId:string;
    @ApiProperty()
    subject:string;
    @ApiProperty()
    description:string;
    @ApiProperty()
    reason:string;
    @ApiProperty()
    addAttachment:string;

    

    
}
