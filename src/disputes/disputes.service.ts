import { Inject, Injectable } from '@nestjs/common';
import { MongoService } from 'src/mongo/mongo.service';
import { CollectionNames, Constants } from 'src/util/constants';
import { CreateDisputeDto } from './dto/create-dispute.dto';
import { RequestContext } from 'src/interceptors/request-context';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class DisputesService {

  constructor(private readonly mongoService: MongoService,
     @Inject("marketservice-tcp") private readonly tcpService: ClientProxy,) { }

 


  async getDisputes() {
    try {
        let checkQuerry: any = {
         $and:[
            {category:{$eq:''}},
            {isDeleted: { $eq: false }},
         ],
            
        }
        let findProducts = await this.mongoService.findWithQueryAndSort(CollectionNames.disputes, checkQuerry,"createdOn","desc");
        if (findProducts) {
            return this.mongoService.createApiResponse(true, findProducts, "Successfully data found");
        } else {
            return this.mongoService.createApiResponse(false, {},"No data found");
        }
    } catch (err) { 

        return this.mongoService.createApiResponse(false,"Something went wrong")
    }
  }

  async getDisputeDetail() {
    try {
        let checkQuerry: any = {
         $and:[
            {category:{$eq:''}},
            {isDeleted: { $eq: false }},
         ],
            
        }
        let findProducts = await this.mongoService.findWithQueryAndSort(CollectionNames.disputes, checkQuerry,"createdOn","desc");
        if (findProducts) {
            return this.mongoService.createApiResponse(true, findProducts, "Successfully data found");
        } else {
            return this.mongoService.createApiResponse(false, {},"No data found");
        }
    } catch (err) { 

        return this.mongoService.createApiResponse(false,"Something went wrong")
    }
  }


  async addDispute(createDisputeDto: CreateDisputeDto) {
    createDisputeDto.orderId= RequestContext.currentUserId();
    let response = await this.tcpService.send(Constants.ADD_DISPUTE_PATTERN, createDisputeDto).toPromise();
    return response;
}
}
