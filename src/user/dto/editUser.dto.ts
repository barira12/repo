import { ApiProperty } from "@nestjs/swagger";
import { PhoneNumberModel } from "src/auth/DTO/signup-dto";

export class EditUserDTO {
  
    @ApiProperty()
    photoUrl: string;
    @ApiProperty()
    name: string;
    @ApiProperty()
    email: string;
    @ApiProperty()
    phoneNumber: PhoneNumberModel;
    @ApiProperty()
    countryCode:string;
    
}