import { Controller, Get, Post, Body, Patch, Param, Delete, Put } from '@nestjs/common';
import { UserService } from './user.service';
import { EditUserDTO } from './dto/editUser.dto';


@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

 

  @Put()
  async editUser(@Body() editUserDTO: EditUserDTO) {
    return this.userService.editUser(editUserDTO);
  }

}
