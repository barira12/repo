import { Injectable } from '@nestjs/common';
import { EditUserDTO } from './dto/editUser.dto';
import { CollectionNames } from 'src/util/constants';
import { RequestContext } from 'src/interceptors/request-context';
import { I18nContext, I18nService } from 'nestjs-i18n';
import { MongoService } from 'src/mongo/mongo.service';
import { UtilService } from 'src/util/util.service';

@Injectable()
export class UserService {


  constructor(
    private readonly mongoService: MongoService,
    private readonly i18n: I18nService,
    private util:UtilService


  ){

  }

  async editUser(editUserDTO: EditUserDTO) {
    try {
        let findUser: any = await this.mongoService.findById(CollectionNames.Users, RequestContext.currentUserId());
        if (findUser) {
            findUser.email = editUserDTO.email;
            findUser.name = editUserDTO.name;
            findUser.phoneNumber.dialCode = editUserDTO.phoneNumber.dialCode;
            findUser.phoneNumber.number = editUserDTO.phoneNumber.number;
            findUser.photoUrl = editUserDTO.photoUrl;
            findUser.countryCode = editUserDTO.countryCode;
            let updateUser = await this.mongoService.update(CollectionNames.Users, findUser, findUser._id, true);
            return this.mongoService.createApiResponse(true, updateUser, this.i18n.t('lang.success', {
                lang: I18nContext.current().lang,
            }),);
        } else {
            return this.mongoService.createApiResponse(false, {}, this.i18n.t('lang.noUserFound', {
                lang: I18nContext.current().lang,
            }),)
        }
    } catch (error) {
        return this.mongoService.createApiResponse(false, error, this.i18n.t('lang.somethingwentwrong', {
            lang: I18nContext.current().lang,
        }),);
    }
}

 
}
