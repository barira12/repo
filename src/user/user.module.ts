import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongoModule } from 'src/mongo/mongo.module';
import { UtilModule } from 'src/util/util.module';

@Module({
  controllers: [UserController],
  providers: [UserService],
  imports:[MongoModule,UtilModule],
})
export class UserModule {}
