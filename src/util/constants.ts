export class Constants {

  static SERVER_ERROR: string = 'Something Went Wrong.';
  public static dbName: string = 'All Padel';
  // public static AGORA_APPID = '8552af76154a40aa9416aa3edb8b2a2e';
  // public static APP_CERTIFICATE = '70fd691147c34f2d9f21f84052cb5017';
  // static GET_COMMUNITIES_BY_ID  = { cmd: "getcommunitybyid" }; 
  static ADD_ORDER_PATTERN = { cmd: 'addorder' };
  static CANCEL_ORDER_PATTERN = { cmd: 'cancelorder' };
  static CREATE_ORDER_INVOICE_PATTERN = { cmd: 'createorderinvoice' };
  static GET_USER_ORDERS_PATTERN = { cmd: 'getuserorders' };
  static ADD_PRODUCT_PATTERN = { cmd: 'addproduct' };
  static ADD_REVIEW_PATTERN = { cmd: 'addreview' };
  static ADD_STORE_PATTERN = { cmd: 'createstore' };
  static UPDATE_STORE_PATTERN = { cmd: 'updatestore' };
  static ADD_PRODUCT_TO_CART = { cmd: 'addproducttocart' };
  static REMOVE_PRODUCT_CART = { cmd: 'removeproductfromcart' };
  static DELETE_PRODUCT_FROM_CART = { cmd: 'deleteproductfromcart' };
  static CLEAR_CART = { cmd: 'clearcart' };
  static GET_USER_CART = { cmd: 'getusercart' };
  static GET_USER_WISHLIST ={cmd:'getUserWishlist'};
  static ADD_PRODUCT_TO_WISHLIST={cmd:'addProducttowishlist'};
  static REMOVE_PRODUCT_FROM_WISHLIST={cmd:'removeproductfromwishlist'};
  static ADD_DISPUTE_PATTERN ={cmd:'adddispute'};
  
}






export enum OrderStatus {
  Pending = "Pending",
  // InProcess = "In Process",
  InProgress = "In Progress",
  ReadyToShip = "Ready To Ship",
  Shipped = "Shipped",
  Delivered = "Delivered",
  Cancelled = "Cancelled",
  Returned = "Returned"
}




export enum CollectionNames {
  Users = 'users',
  Emails = 'emails',
  Wallet = "Wallet",
  reactions = "reactions",
  Voucher = "vouchers",
  orders = "orders",
  reviews = "reviews",
  products = "products",
  Store = "store",
  disputes = "disputes"
}

export enum TemplateNames {
  ForgotPassword = 'forgotPassword',
  WelcomeEmail = 'welcomeEmail',
  VerifyEmail = 'verifyEmail',
}

export class Credentials {
  static type: string = 'service_account';
  static project_id: string = 'allPadel-e23c8';
  static private_key: string =
    '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC9iu03dakfQl6v\nOpEPlDBMuO1czZvF428qkRIUgmgJaJxchQzE8oHJh1we414porQDnLO3X0tFSZ2e\n1f3knT/THjMF3IEfpbs+2fvEbjWo99b0W7RKeofk0ax/O86oogvp/280A2+YA+Cr\ncE0Y2nYJFSu4qLftHJtPMEMgbeY/EMOXJ4489Z0bOnuXOrWGme457rttXbyodA/3\naHQWoOpES/y3Sc076cpAG9TR2/YdyXJXdqDODbJccpIw2QDWN5Pqbbg+rzwbRkbE\n3ui1KIA+TJcpU+3DYmvg2rDONN89OQhcDBMF7TghlqrdBUkINyoSBwL1mQJNeNqa\nel3RdU0lAgMBAAECggEAA0c7Whm1Xp6Jv7MeHixcd389T70YpCAOZKiUrPkiboM6\nLRl6ApAFMaSfMcOR9dtkDKnxq8rApQakbMJbvVAA8hhUHpZVP2Rg7DHd+mRDllQP\nCjPp1t3uqsT7nEm/BcijtxzdYgNrMzBh+uUa2EKzBu4wTuXQJJA5mXGGv7FW6aFL\ndIXfGiRIhXHXWSjWRzh0uvluheSZ84qz7e/mZGUL9NiuP+LXlIRSHbZbZEZayVnn\nNtxCKBJ2lElK6jhZdvJA2nlbDuenmyHmhqh2B2zjpub+1uM8dN9+0ERuzUubNZpu\nsNxI0Ed9QqqzyCmyf20/qpVjdTu2XX/TBO5kf0t0oQKBgQDtgDtX+CMxaL3rWo0q\njoEs+5gG4II24FMR0Dq0D67e0MMnPY3eWO+LVSYYJDG6CEWkZdeK8m1sTITzc4O2\n5QAHtkOpAYsmZ4BomM78qWpeE4WNDlABxaZZfBCVrMJkls68kLGw5eUoOha0H9Mo\nx+xyCzkeu7O+l/MiufOxD/dpsQKBgQDMTmhmmPoFOR+sWuZbpAcmU6UTVWIp8RXu\nw4E9a3ZQIjKmmMSF5gyCcHFdrdXPSovrlV5DSreAgDRD8gJcSDuLlnlwLqN2EzFW\nFbS/6wNWuV4w3XRRJZue0AMeA+G1b7pLDPfCxjAkMgnbN0WgKqro2Q6zak0+6x5j\nWg2F3hKDtQKBgQCvffNbu0T1+3lqcQyih4432OirHDXbqsJ3BTB7YKNMjWHGmxs7\nUugeQprVd9kup3IeWgp850mnnpsW8SJAgNIW4Lz6IuPZSt19bHx2AhFQ1of2hL7l\nsFCNCrmOf4tcnHrCpwBFwHol0VZ5XpK/LYsvcc+RQA0/JjRIUrMZAL1G4QKBgQCc\nGThJU46L2RZrINSqpgvUa8pPmEg1VX91eyjO3cF6/nYGtehJifh63SCw+7XF+OYp\nLGnmLjtAUjQcydal3YapY2ILpYa9G3LxnAoLLKvTYjQmT72kloBtee83yq10bBZB\na3oN9lEoBpdjR7rJ4m/NErLZwFmevhPswwxcAM3SlQKBgQDiZcMo5p60OB47Dxz5\nqs/QC/wevG8SZ0VPvQQgtesYVw0hSUYyYC4IIl7a/CFNlCloAGxTY9Vw//jkUjgI\n54AwO2mFp3vjgYY3kJWUdUzpnj18J5VJGq/Rf3MMHw01xSpfGnQJT6kSC2jQfBqE\nQAgR5yfwKXgpcfqp5C7PixRWYA==\n-----END PRIVATE KEY-----\n';
  static client_email: string =
    'firebase-adminsdk-a8w8q@allPadel-e23c8.iam.gserviceaccount.com';
  static databaseURL: string =
    'https://yallago-e23c8-default-rtdb.firebaseio.com/';
  static storageURL: string = 'gs://wetrain-ee87f.appspot.com';

}
