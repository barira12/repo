import { Injectable } from '@nestjs/common';


import { ResponseModel } from './model/response.model';
import { EntityLogModel, FindDataModel, Operation } from 'packages/mongodb-crud-operations';
import { RequestContext } from 'src/interceptors/request-context';


@Injectable()
export class UtilService {
    getObject(arg0: boolean, arg1: string, response: any) {
      throw new Error('Method not implemented.');
    }
    getEntityLogUpdateMany(arg0: { isStoreActive: boolean; }, dbName: string, products: any, Update: Operation, arg4: any, arg5: string, arg6: { storeId: { $eq: any; }; }) {
      throw new Error('Method not implemented.');
    }
    insertOne(arg0: string, CreateLedgerDTO: any) {
        throw new Error('Method not implemented.');
    }

    public getfindDataModelQuery(dbName: string, entityName: string, query: any, sortField?: string, sortType?: number, limit?: number, offset?: number) {
        let findData:any = new FindDataModel();
        findData.dbName = dbName;   
        findData.entityName = entityName;
        findData.query = query;
        findData.limit = limit;
        findData.offset = offset;
        findData.sortObject = {};
        findData.sortObject[sortField] = sortType
        return findData;
    }


    public getfindDataModelId(dbName: string, entityName: string, id: string) {
        let findData = new FindDataModel();
        findData.dbName = dbName;
        findData.entityName = entityName;
        findData.id = id;
        return findData;
    }

    public getEntityLog(data: any, dbName: string, entityName: string, operation: Operation, userId: string, userName: string, entityId?: string): EntityLogModel {
        let entityLog = new EntityLogModel();
        entityLog.date = new Date().toISOString();
        entityLog.dbName = dbName;
        entityLog.entityName = entityName;
        entityLog.fieldValues = this.resolveFields(data);
        entityLog.operation = operation;
        entityLog.userId = userId;
        entityLog.userName = userName;
        entityLog.entityId = entityId;

        return entityLog;
    }

    public resolveFields(data) {
        let objectKeys = Object.keys(data);
        let fieldValues = [];
        for (let key of objectKeys) {
            let tempData = {};
            tempData[key] = data[key];
            fieldValues.push(tempData);
        }
        return fieldValues;
    }

    public getResponseObject(isSuccess: boolean, message?: string, data?: any) {
        return new ResponseModel(isSuccess,data,message);
    }
    

    public calculateBytes(x) {
        const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        let l = 0, n = parseInt(x, 10) || 0;

        while (n >= 1024 && ++l) {
            n = n / 1024;
        }

        return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
    }
    getUserIdFromCurrentRequest() {
        let req = RequestContext.currentRequest();
        let userId: string;
        if (req) {
            userId = req['userId'];
        }
        return userId;
    }
    diff_minutes(dt2, dt1) {
        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.abs(Math.round(diff));
    }
}


