
export enum Collections {
    postfeeds = "postfeeds",
    users = "users",
    stores = "stores",
    emails = "emails",
    license = "license",
    requests = "requests",
    config = "config",
    reports = "reports",
    posts = "posts",
    liveLocation = "liveLocation",
    orders = "orders",
    hyperbcorders = "hyperbcorders",
    hyperbcorderscompletion = "hyperbcorderscompletion",
    products = "products",
    auctions = "auctions"
  }
  
  export enum TemplateNames {
      ForgotPassword = "forgotPassword",
      WelcomeEmail = "welcomeEmail",
      VerifyEmail = "verifyEmail",
      
  }
  
  
  export enum requestStatus {
      pending = "pending",
      applied = "applied"
  }