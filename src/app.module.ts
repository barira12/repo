import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { AuthenticationModule } from 'packages/authentication-service';
import { MongoBaseModule } from 'packages/mongodb-crud-operations';
import { UtilModule } from './util/util.module';
import { UserModule } from './user/user.module';
import { InterceptorsModule } from './interceptors/interceptors.module';
import { RequestMiddleware } from './interceptors/request.middleware';
import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AcceptLanguageResolver, I18nModule, QueryResolver } from 'nestjs-i18n';
import * as path from 'path';
import { UserController } from './user/user.controller';
import { MongoModule } from './mongo/mongo.module';
import { VouchersModule } from './vouchers/vouchers.module';
import { OrdersModule } from './orders/orders.module';
import { StoresModule } from './stores/stores.module';
import { CartModule } from './cart/cart.module';
import { VouchersController } from './vouchers/vouchers.controller';
import { OrdersController } from './orders/orders.controller';
import { StoresController } from './stores/stores.controller';
import { CartController } from './cart/cart.controller';
import { DisputesModule } from './disputes/disputes.module';
import { WishlistModule } from './wishlist/wishlist.module';





@Module({
  imports: [
    I18nModule.forRoot({
      fallbackLanguage: 'en',
      loaderOptions: {
        path: path.join(),
        watch: true,
      },
      resolvers: [
        { use: QueryResolver, options: ['lang'] },
        AcceptLanguageResolver,
      ],
    }),
    AuthModule, AuthenticationModule, MongoBaseModule, UtilModule, UserModule,
    VouchersModule, MongoModule, OrdersModule, StoresModule, CartModule,
     InterceptorsModule, MongoModule, DisputesModule, WishlistModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule
  implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(RequestMiddleware)
      .exclude()
      .forRoutes({ path: "auth/getUser", method: RequestMethod.GET },
        { path: "auth/updateUser", method: RequestMethod.PUT },
        { path: "auth/createUser", method: RequestMethod.PUT },
        { path: "auth/deleteaccount", method: RequestMethod.DELETE },
        { path: "auth/sendVerifyEmail", method: RequestMethod.POST }
        , { path: "auth/verifyEmail", method: RequestMethod.POST }
        , { path: "auth/changePassword", method: RequestMethod.PUT },
        VouchersController, 
        UserController,
        OrdersController,
        StoresController,
        CartController
      
      );

  }
}




