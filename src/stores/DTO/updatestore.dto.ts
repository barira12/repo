import { ApiProperty } from "@nestjs/swagger";
import { LocationModel, SocialMediaHandleModel } from "./addstore.dto";

export class BackAccountModel{
    @ApiProperty()
    bankName:string;
    @ApiProperty()
    ibanNo:string;
}
export class DocumentModel {
    @ApiProperty()
    name: string;
    @ApiProperty()
    type: string;
    @ApiProperty()
    url: string;
}

export class UpdateStoreDTO {
    @ApiProperty()
    storeId:string;
    ownerId:string;
    @ApiProperty()
    coverUrl: string;
    @ApiProperty()
    profileUrl: string;
    @ApiProperty()
    storeName: string;
    @ApiProperty()
    storeSymbol: string;
    @ApiProperty()
    category: string;
    @ApiProperty()
    storeDescription: string;
    @ApiProperty()
    locationAndAddress: LocationModel;
    @ApiProperty({isArray:true,type:SocialMediaHandleModel})
    socialMediaHandles: SocialMediaHandleModel[];
    @ApiProperty({isArray:true,type:BackAccountModel})
    bankAccounts: BackAccountModel[];
    @ApiProperty({isArray:true,type:typeof('')})
    walletAddresses: string[];
    @ApiProperty({isArray:true,type:DocumentModel})
    documents: DocumentModel[];
    // @ApiProperty()
    // authentication:AuthenticationModel;
}