import { ApiProperty } from "@nestjs/swagger";

export class SocialMediaHandleModel {
    @ApiProperty()
    name: string;
    @ApiProperty()
    url: string;
  }
  
export class Address {
    @ApiProperty()
    locationOfBusiness: string;
    @ApiProperty()
    city: string;
    @ApiProperty()
    state: string;
    @ApiProperty()
    zipcode: string;
  }
  
  export class LocationDTO {
    @ApiProperty()
    type: string;
    @ApiProperty({type:Number,isArray:true})
    coordinates: number[]
  }
  
export class LocationModel {
    @ApiProperty()
    location: LocationDTO;
    @ApiProperty()
    address: Address
  }
export class AddStoreDTO {
    @ApiProperty()
    coverUrl: string;
    @ApiProperty()
    profileUrl: string;
    @ApiProperty()
    storeName: string;
    @ApiProperty()
    storeSymbol: string;
    @ApiProperty()
    storeType: string;
    @ApiProperty()
    storeDescription: string;
    ownerId: string;
    @ApiProperty()
    locationAndAddress: LocationModel;
    @ApiProperty({ isArray: true, type: SocialMediaHandleModel })
    socialMediaHandles: SocialMediaHandleModel[];
  }
  