import { Controller, Get, } from '@nestjs/common';
import { StoresService } from './stores.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiBearerAuth("access-token")
@ApiTags("stores")
@Controller('stores')
export class StoresController {
  constructor(private readonly storesService: StoresService) { }

  @Get()
  async getStores() {
    return this.storesService.getStores()
  }

}
