import { Module } from '@nestjs/common';
import { StoresService } from './stores.service';
import { StoresController } from './stores.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { MongoModule } from 'src/mongo/mongo.module';

@Module({
  controllers: [StoresController],
  providers: [StoresService, {
    provide: 'marketservice-tcp',
    inject: [ConfigService],
    useFactory: (configService: ConfigService) => {
      let port = configService.get('MARKETPLACE_SERVICE_PORT');
      return ClientProxyFactory.create({ transport: Transport.TCP, options: { host: 'host.docker.internal', port: parseInt(port) } })
    },
  }],
  imports: [ConfigModule, MongoModule,]
})
export class StoresModule { }
