import { Injectable } from '@nestjs/common';
import { MongoService } from 'src/mongo/mongo.service';
import { CollectionNames } from 'src/util/constants';

@Injectable()
export class StoresService {
  constructor(

    private mongo: MongoService,
    // @Inject("marketservice-tcp") private readonly tcpService: ClientProxy,
  ) { }


  async getStores() {
    try {
      let checkQuery: any = {
        $and: [
          { isFeatured: { $eq: true } },
        ]
      };
      let findStore = await this.mongo.findWithQueryAndSort(CollectionNames.Store, checkQuery, "createdOn", "desc",);
      if (findStore) {
        return this.mongo.createApiResponse(true, findStore, "Success");
      } else {
        return this.mongo.createApiResponse(false, {}, "No data found");
      }
    } catch (error) {
      return this.mongo.createApiResponse(false, error, "Something went wrong");
    }

  }





}
