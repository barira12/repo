export interface SocialLoginModel {
    token: string;
    user: SocialUser;
}
export interface SocialUser {
    email: string;
    uid: string;
    phoneNumber:string;
    dialCode: string;
    loginMethod:string;
}

//# sourceMappingURL=sociallogin.model.d.ts.map