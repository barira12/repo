FROM node:lts-alpine
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install --production --silent && mv node_modules ../
COPY ./packages/authentication-service/ /app/node_modules/authentication-service
COPY ./packages/mongodb-crud-operations/ /app/node_modules/mongodb-crud-operations
COPY ./ /app
EXPOSE 3000
RUN chown -R node /usr/src/app
USER node
CMD ["npm", "start"]
